package cn.cup.dmp.beans

case class RptProvinceCityResult(province: String, city: String, ct: Int)

/**
  * 省份，城市，原始请求书，有效请求数，满足广告请求数，参与竞价数。成功竞价数。每次消费金额，每次成本，展示了，点击量
  *
  * @param province
  * @param city
  */
case class RptAreaDistributeResult(province: String,
                                   city: String,
                                   original_request_number: Int,
                                   valid_requests: Int,
                                   meet_condition_request: Int,
                                   partitionbidsNum: Int,
                                   success_bid_Num: Int,
                                   everytime_consump: Double,
                                   everytime_cost: Double,
                                   show_times: Int,
                                   click_times: Int
                                  )
