package cn.cup.dmp.etl

import cn.cup.dmp.config.ConfigHelper
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 读取parquet文件
  */
object Read4Parquet {

  def main(args: Array[String]): Unit = {


    val sparkConf = new SparkConf()
    sparkConf.setAppName("测试读取parquet文件中的内容")
    sparkConf.setMaster("local[*]")

    // RDD | worker -> work
    sparkConf.set("spark.serializer", ConfigHelper.ser)


    val sc = new SparkContext(sparkConf)
    val sQLContext = new SQLContext(sc)

    val dataFrame = sQLContext.read.parquet("D:\\小牛视频\\资料_广告项目\\parquet")

    dataFrame.show()//显示20条，可以自己定义显示的条数

    sc.stop()
  }


}
