package cn.cup.dmp.config

import com.typesafe.config.{Config, ConfigFactory}
import scalikejdbc.config.DBs

object ConfigHelper{
  private lazy  val config: Config = ConfigFactory.load()
  /**
    * spark的序列化方式 Kro
    */
  val ser: String = config.getString("spark.serializer")
  /**
    * 原始文件路径
    */
  val srcfilePath: String = config.getString("filePath")
  /**
    * 初始处理的目标地址
    */
  val destPath: String = config.getString("destPath")
  /**
    * jdbc 的参数
    */
  val driverClass: String = config.getString("db.default.driver")
  val url: String = config.getString("db.default.url")
  val username: String = config.getString("db.default.user")
  val password: String = config.getString("db.default.password")

  val tableName: String = config.getString("jdbc.targettable")
  /**
    * 地域分布表
    */
  val area_Distribute_table: String = config.getString("db.default.kpitable")
  /**
    * 媒体报表统计表
    */
    val app_Analysis_table = config.getString("app_analysis_table")
  /**
    * redis 的配置
    */
  val redis_host: String = config.getString("redis.host")
  val redis_port: Int = config.getInt("redis.port")


  /**
    * scalike 加载配置参数
    */

  val dbSetup: Unit = DBs.setup()


}
