package cn.cup.dmp.utils

/**
  * 使用隐式转换把字符串转化为Int或者double类型
  * @param str
  */
class String2NumFormat(str:String) {
  def toIntx={
    try{
      str.toInt
    }catch{
      case _:Exception => 0
    }
  }
  def toDoublex={
    try{
      str.toDouble
    }catch{
      case _:Exception => 0.0
    }
  }
}
object String2NumFormat{
  implicit def string2Int(str:String)= new String2NumFormat(str)
}