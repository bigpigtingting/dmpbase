package cn.cup.dmp.utils

import cn.cup.dmp.config.ConfigHelper
import redis.clients.jedis.JedisPool

object MyJedisPool {
  lazy private val pool = new JedisPool(ConfigHelper.redis_host,ConfigHelper.redis_port)
  def getRedis() ={
    val redis =  pool.getResource
    redis.select(2)
    redis
  }
}
